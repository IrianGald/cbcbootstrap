##Estructura de archivos


En la carpeta **node_modules/bootstrap** están los archivos de bootstrap, sería de reemplazar tu módulo por el que te envío en este repositorio.

En la carpeta **htmlsmartdesk** se encuentran las últimas versiones de los HTML's y los otros archivos como las webfonts, otros estilos, logo, etc.:
##
```
htmlsmartdesk/
│   coberturas.html
│   favicon.ico
│   inicio.html
│   portafolio.html
│
├───css/
│       styles.css
│       wfonts.css
│
├───fonts/
│       onelink.eot
│       onelink.svg
│       onelink.ttf
│       onelink.woff
│
├───img/
│       cbc-logo.png
│       img-placeholder.png
│
└───js/
        jquery-3.2.1.slim.min.js
        jquery-3.2.1.slim.min.map
```
